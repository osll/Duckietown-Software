#!/bin/bash

script_dir=$(dirname "$BASH_SOURCE")
source $script_dir/../catkin_ws/devel/setup.bash

export ROS_HOSTNAME="${HOSTNAME//_}.local"
export ROSLAUNCH_SSH_UNKNOWN=1

bot_name="duckmobile"
if [ -z "$1" ]; then
	if [[ ! -v VEHICLE_NAME ]]; then
		printf "Specify bot name ($bot_name): "
		read inp
		if [ ! -z "$inp" ]; then
			bot_name=$inp
		fi
	else
		echo "Using name from env var"
		bot_name=$VEHICLE_NAME
	fi
else
  bot_name=$1
fi

echo "Enjoy your $bot_name"
export VEHICLE_NAME="$bot_name"
