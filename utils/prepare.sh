#!/bin/bash
if [[ "$#" != "1" ]]
then
    echo "using: $0 <bot_name>"
else
	export ROS_MASTER_URI=http://$1.local:11311
	export ROS_HOSTNAME="$HOSTNAME.local"
	export ROSLAUNCH_SSH_UNKNOWN=1
	export VEHICLE_NAME="$1"
	source ~/git/gitlab-Duckietown-Software/catkin_ws/devel/setup.bash
fi
